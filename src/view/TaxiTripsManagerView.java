package view;

import java.util.Scanner;


import controller.Controller;
import model.data_structures.IList;
import model.data_structures.LinkedList;
import model.logic.TaxiTripsManager;
import model.vo.Servicio;
import model.vo.TaxiTaller;

/**
 * view del programa
 */
public class TaxiTripsManagerView 
{

	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			//imprime menu
			printMenu();

			//opcion req
			int option = sc.nextInt();

			switch(option)
			{

			case 1: // cargar informacion a procesar

				//imprime menu cargar
				printMenuCargar();

				//opcion cargar
				int optionCargar = sc.nextInt();

				//directorio json
				String linkJson = "";
				switch (optionCargar)
				{
				//direccion json pequeno
				case 1:

					linkJson = TaxiTripsManager.DIRECCION_SMALL_JSON;
					break;

					//direccion json mediano
				case 2:

					linkJson = TaxiTripsManager.DIRECCION_MEDIUM_JSON;
					break;

					//direccion json grande
				case 3:

					linkJson = TaxiTripsManager.DIRECCION_LARGE_JSON;
					break;
				}

				System.out.println("Datos cargados: " + linkJson);
				//Memoria y tiempo
				long memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				long startTime = System.nanoTime();

				//Cargar data
				Controller.cargarSistema(linkJson);

				//Tiempo en cargar
				long endTime = System.nanoTime();
				long duration = (endTime - startTime)/(1000000);

				//Memoria usada
				long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				System.out.println("Tiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB");

				break;


			case 2: 
				
				Controller.darInformacion();

				break;

			case 3: 

				//Duracion de la consulta
				System.out.println("Ingrese el id del taxi (pId)");
				String pId = sc.next();
				TaxiTaller temp = Controller.darInformacionAsociadaTaxi(pId);
				if(temp == null) 
				{
					System.out.println("no se encontro un taxi asociado a ese id");
				}
				else
				{
					System.out.println("la informacionAsociada al taxi "+ temp.getTaxiId() );
					System.out.println("la compa�ia a la que pertenece es " + temp.getEmpresaTaxi());
					System.out.println("ha  recorrido una distancia de " + temp.getTotalDistancia());
					System.out.println("ha recogido una cantidad de dinero igual a " + temp.getTotalDinero() );
					System.out.println("durante un tiempo de "+ temp.getTotalTiempo());
					System.out.println("con un total de " + temp.getServiciosAsociados()+ " servicios");
				}

				break;

			case 4: 
				String pIdMenor = "";
				String pIdMayor = "";
				System.out.println("ingrese el id menor para el rango de busqueda");
				pIdMenor = sc.next();
				System.out.println("ingrese el id mayor para el rango de busqueda");
				pIdMayor = sc.next();
				LinkedList<String> taxiIds = Controller.darLLavesTaxisEnRango(pIdMenor, pIdMayor);
				if(taxiIds == null) 
				{
					System.out.println("no se encontro ningun taxi en el rango de busqueda");
				}
				else
				{
					for(int i = 0; i < taxiIds.size();i++) 
					{
						System.out.println("el id del taxi "+ i + " es "+ taxiIds.get(i));
					}
				}

				break;
				
			case 9: 
				fin=true;
				sc.close();
				break;

			}
		}
	}
	/**
	 * Menu 
	 */
	private static void printMenu() //
	{
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 7----------------------");
		System.out.println("Iniciar la Fuente de Datos a Consultar :");
		System.out.println("1. Cargar toda la informacion del sistema de una fuente de datos (small, medium o large).");

		
		System.out.println("2. Obtener informacion general pregunta 2 taller");
		System.out.println("3. Obtener la informacion de un taxi dado su id pregunta 3 taller");
		System.out.println("4. Obtener los ids que esten en un rango dado pregunta 4 taller");
		
		System.out.println("9. Salir");
		System.out.println("Ingrese el numero de la opcion seleccionada y presione <Enter> para confirmar: (e.g., 1):");

	}

	private static void printMenuCargar()
	{
		System.out.println("-- Que fuente de datos desea cargar?");
		System.out.println("-- 1. Small");
		System.out.println("-- 2. Medium");
		System.out.println("-- 3. Large");
		System.out.println("-- Ingrese el numero de la fuente a cargar y presione <Enter> para confirmar: (e.g., 1)");
	}

}
