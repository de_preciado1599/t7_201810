package controller;

import api.ITaxiTripsManager;
import model.data_structures.IList;
import model.data_structures.LinkedList;
import model.logic.TaxiTripsManager;
import model.vo.Servicio;
import model.vo.TaxiTaller;

public class Controller 
{
	/**
	 * modela el manejador de la clase l�gica
	 */
	private static ITaxiTripsManager manager =new TaxiTripsManager();

	//Carga El sistema
	public static boolean cargarSistema(String direccionJson)
	{
		return manager.cargarSistema(direccionJson);
	}
	
	public static LinkedList<String> darLLavesTaxisEnRango(String pIdMenor, String pIdMayor)
	{
		return manager.darLLavesTaxisEnRango(pIdMenor, pIdMayor);
	}

	public static TaxiTaller darInformacionAsociadaTaxi(String pId) 
	{
		return manager.darInformacionAsociadaTaxi(pId);
	}
	
	public static void darInformacion() 
	{
		 manager.darInformacion();
	}
}
