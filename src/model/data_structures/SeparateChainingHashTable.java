package model.data_structures;

import model.data_structures.SeparateChainingHashTable.SequentialSearchST.Node;

public class SeparateChainingHashTable<Key extends Comparable<Key>, Value> {

	private int n;

	private int m;

	private SequentialSearchST<Key, Value>[] st;


	public int size(){
		return n;
	}

	public int m(){
		return m;
	}


	public SeparateChainingHashTable(int m) {
		this.m = m;
		st = (SequentialSearchST<Key, Value>[]) new SequentialSearchST[m];		
		for (int i = 0; i < m; i++){
			st[i] = new SequentialSearchST();
		}
	}

	/**
	 * retorna el valor obtenido por la llave
	 * @param key
	 * @return
	 */
	public Value getValue(Key key){
		return (Value) st[hash(key)].get(key);
	}

	public void put(Key key, Value val){
		if (val == null) {
			delete(key);
			return;
		}
		// double table size if average length of list >= 10
		if (n >= 2*m && m!=1){
			resize(2*m);
		}
		int i = hash(key);
		if(!st[i].contains(key))
		{
			n++;
		}
		st[i].put(key, val);

	} 

	public boolean delete(Key key) {
		boolean rta=false;
		if (key == null){
			rta = false;
		}

		int i = hash(key);
		if (st[i].contains(key)){
			st[i].delete(key);
			n--;
			rta=true;
		}

		// halve table size if average length of list <= 2
		if ( n <= 2*m && m!=1) resize(m/2);
		return rta;
	}

	public boolean contains(Key key){
		int i = hash(key);
		Node actual = st[i].first;
		for ( Node x = actual ; x != null ; x = x.next) {
			if(x.key.equals(key))
				return true;
		}
		return false;
	}


	private int hash(Key key)
	{
		return (key.hashCode() & 0x7fffffff) % m; 
	}

	private void resize(int chains) {
		SeparateChainingHashTable<Key, Value> temp = new SeparateChainingHashTable<Key, Value>(chains);
		for (int i = 0; i < m; i++) {
			for (Node x = st[i].first ; x != null ; x = x.next) {
				temp.put(((Key) x.key), ((Value) x.value));
			}
		}
		this.m  = temp.m;
		this.n  = temp.n;
		this.st = temp.st;
	}


	public class SequentialSearchST<Key, Value>{

		private Node first;

		class Node{

			Node next;
			Value value;
			Key key;

			public Node(Key key, Value value, Node next){
				this.key = key;
				this.value = value;
				this.next= next;
			}
			//TERMINA LA CLASE NODO
		}

		public Value get(Key key){
			for (Node x = first;  x != null ; x = x.next) {
				if(x.key.equals(key))
					return x.value;
			}
			return null;
		}

		public void put(Key key, Value value){
			if(first == null){
				first = new Node(key, value, null);
				return;
			}
			for (Node x = first; x != null; x = x.next) {
				if(x.key.equals(key)){
					x.value = value;
					return;
				}
			}
			first = new Node(key, value, first);
		}

		public void delete(Key key){
			if(first.next == null && first.key.equals(key))
			{
				first = null;
				return;
			}
			else if(first.key.equals(key) && first.next != null){
				first = first.next;
				return;
			}
			for (Node x = first; x != null && x.next!=null; x = x.next) {
				if(x.next.key.equals(key)){
					x.next= x.next.next;
					return;
				}
			}
		}


		public boolean contains(Key key){
			for ( Node x = first ; x != null ; x = x.next) {
				if(x.key.equals(key))
					return true;
			}
			return false;
		}

		//TERMINA LA CLASE CHAINING
	}
	//TERMINA LA CLASE SEPARATE CHAINING
}
