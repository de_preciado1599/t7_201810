package model.logic;

import java.io.FileInputStream;
import java.io.InputStreamReader;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import model.data_structures.LinkedList;
import model.data_structures.RedBlackBST;
import model.vo.ServicioResumen;
import model.vo.TaxiTaller;

public class TaxiTripsManager implements api.ITaxiTripsManager {
	
	//_____________________________________________________________________________
	//Constantes
	//_____________________________________________________________________________
	
	/**
	 * 
	 */
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	
	/**
	 * 
	 */
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	
	/**
	 * 
	 */
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";
	
	//___________________________________________________________________________________
	//Attributes
	//___________________________________________________________________________________
		
	/**
	 * @author Daniel Preciado
	 * representa un arbol que usa como llave un String que reprenta el id
	 * al cual pertenece al taxi, de esta forma vamos a tener un arbol
	 * ordenado mediante los identificadores del taxi
	 */
	private RedBlackBST<String, TaxiTaller> arbolTaxis;

	//__________________________________________________________________________________
	//carga
	//___________________________________________________________________________________
	@Override
	/**
	 * @param direccionjson 
	 */
	public boolean cargarSistema(String direccionJson) 
	{
		arbolTaxis = new RedBlackBST<>();
		boolean rta= false;
		if(direccionJson.equals(DIRECCION_LARGE_JSON))
		{
			int x= 2;
			while(x<9){
				String route = "./data/taxi-trips-wrvz-psew-subset-0"+x+"-02-2017.json";
				System.out.println(route);
				loadFileJson(route);
				x++;
			}			
		}
		else
		{
			loadFileJson(direccionJson);
			rta= true;
		}
		return rta;
	}



	/**
	 * @author Daniel Preciado
	 * @param file
	 */
	public void loadFileJson(String file){
		try{
			JsonReader jsonReader = new JsonReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
			Gson gson = new GsonBuilder().create();
			jsonReader.beginArray();
			while (jsonReader.hasNext())
			{
				ServicioResumen s = gson.fromJson(jsonReader, ServicioResumen.class);
				//carga el arbol de taxisTaller
				TaxiTaller temp = arbolTaxis.get(s.getTaxi_id());
				if(temp == null) 
				{
					if(s.getCompany()== null ) 
					{
						temp = new TaxiTaller(s.getTaxi_id(), "Independent Owner", s.getTrip_total(), s.getTrip_seconds(), s.getTrip_miles());
					}
					else
					{
						temp = new TaxiTaller(s.getTaxi_id(), s.getCompany(), s.getTrip_total(), s.getTrip_seconds(), s.getTrip_miles());
					}
					arbolTaxis.put(temp.getTaxiId(), temp);
				}
				else
				{
					temp.aumentarDinero(s.getTrip_total());
					temp.aumentarDistancia(s.getTrip_miles());
					temp.aumentarTiempo(s.getTrip_seconds());
				}
			}
			System.out.println("el arbol tiene un tamanio de "+ arbolTaxis.size());
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}



	@Override
	public void darInformacion() {
		// TODO Auto-generated method stub
		System.out.println("total de taxis en el arbol = " + arbolTaxis.size());
		System.out.println("La altura del arbol es = "+ arbolTaxis.height());
		System.out.println("");
		
	}



	@Override
	public TaxiTaller darInformacionAsociadaTaxi(String pId) {
		// TODO Auto-generated method stub
		TaxiTaller temp = arbolTaxis.get(pId);
		return temp;
	}



	@Override
	public LinkedList<String> darLLavesTaxisEnRango(String pIdMenor, String pIdMayor) 
	{
		System.out.println("entre aqui");
		// TODO Auto-generated method stub
		LinkedList<String> retorno = new LinkedList<>();
		for (String key : arbolTaxis.keys(pIdMenor, pIdMayor)) 
		{
			TaxiTaller temp = arbolTaxis.get(key);
			retorno.add(temp.getTaxiId());
		}
		
		System.out.println("sali aqui" +retorno.size());
		return retorno;
	}


	

}
