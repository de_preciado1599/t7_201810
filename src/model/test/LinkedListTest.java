package model.test;

import junit.framework.TestCase;
import model.data_structures.LinkedList;

public class LinkedListTest extends TestCase {
	private LinkedList<String> listaNombres;

	private void setupEscenario(){
		listaNombres = new LinkedList<String>();
		listaNombres.add("Sebastian");
		listaNombres.add("Andr�s");
		listaNombres.add("Martha");
		listaNombres.add("Mar�a");
		listaNombres.add("Daniela");
		listaNombres.add("Antonio");
		listaNombres.add("Alvaro");
	} 


	private void setupEscenario3(){
		listaNombres = new LinkedList<String>();
		listaNombres.add("Sebastian");
		listaNombres.add("Andr�s");

	}



	private void setupEscenario2(){
		listaNombres = new LinkedList<String>();
		assertEquals("el nombre no es el esperado", "Andr�s", listaNombres.get(1));
		listaNombres.add("Sebastian");
		assertNull(listaNombres.get(1));
		assertEquals(0, listaNombres.size());
	} 




	public void testReset(){
		setupEscenario();
		listaNombres.reset();

	}

	public void testAgregarObtener(){
		setupEscenario();
		assertEquals("el nombre no es el esperado", "Sebastian", listaNombres.get(1));
		assertEquals("el nombre no es el esperado", "Andr�s", listaNombres.get(2));
		assertEquals("el nombre no es el esperado", "Martha", listaNombres.get(3));
		assertEquals("el nombre no es el esperado", "Mar�a", listaNombres.get(4));
		assertEquals("el nombre no es el esperado", "Daniela", listaNombres.get(5));
		assertEquals("el nombre no es el esperado", "Antonio", listaNombres.get(6));
		assertEquals("el nombre no es el esperado", "Alvaro", listaNombres.get(7));
	}
	public void testAgregarObtener2(){
		setupEscenario();
		assertEquals("el nombre no es el esperado", "Sebastian", listaNombres.get("Sebastian"));
		assertEquals("el nombre no es el esperado", "Andr�s", listaNombres.get("Andr�s"));
		assertEquals("el nombre no es el esperado", "Martha", listaNombres.get("Martha"));
		assertEquals("el nombre no es el esperado", "Mar�a", listaNombres.get("Mar�a"));
		assertEquals("el nombre no es el esperado", "Daniela", listaNombres.get("Daniela"));
		assertEquals("el nombre no es el esperado", "Antonio", listaNombres.get("Antonio"));
		assertEquals("el nombre no es el esperado", "Alvaro", listaNombres.get("Alvaro"));
	}
	public void testEliminarPrimero(){
		setupEscenario();
		listaNombres.delete("Sebastian");
		assertEquals("el nombre no es el esperado", "Andr�s", listaNombres.get(1));
		assertEquals("el nombre no es el esperado", "Martha", listaNombres.get(2));
		assertEquals("el nombre no es el esperado", "Mar�a", listaNombres.get(3));
		assertEquals("el nombre no es el esperado", "Daniela", listaNombres.get(4));
		assertEquals("el nombre no es el esperado", "Antonio", listaNombres.get(5));
		assertEquals("el nombre no es el esperado", "Alvaro", listaNombres.get(6));
	}	
	public void testTamanio(){
		setupEscenario();
		assertEquals(7, listaNombres.size());
		listaNombres.delete("Sebastian");
		listaNombres.delete("Alvaro");
		assertEquals(5, listaNombres.size());
	}

	public void testBuscarUltimo(){
		setupEscenario();
		assertEquals("el nombre no es el esperado", "Alvaro", listaNombres.buscarUltimo().darElemento());
	}

	public void testEliminarPrimero2(){
		setupEscenario3();
		listaNombres.delete("Sebastian");
		assertNull(listaNombres.get("Sebastian"));
		assertEquals("el nombre no es el esperado", "Andr�s", listaNombres.get(0));
	}

//	public void testIterador(){
//		setupEscenario3();
//		assertEquals("el nombre no es el esperado", "Sebastian", listaNombres.next());
//		assertNull("deber�a retornar null", listaNombres.next());
//	}
}
