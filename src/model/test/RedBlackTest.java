package model.test;


import junit.framework.TestCase;
import model.data_structures.RedBlackBST;

public class RedBlackTest extends TestCase {
	
	private RedBlackBST<String, String> cedulas;
	
	private void SetupEscenario(){
		cedulas = new RedBlackBST<String, String>();
		cedulas.put("2030445", "Luis Mar�a Mujica");
		cedulas.put("121", "Luis");
		cedulas.put("123", "andrea");
		cedulas.put("1234", "alejandra");
		cedulas.put("12345", "mar�a");
		cedulas.put("123456", "Sara");
		cedulas.put("1234567", "david");
		cedulas.put("123456789999", "sdsdsdsd");
		cedulas.put("123456789", "karen");
		cedulas.put("12345678910", "mylene");
		cedulas.put("1234567891012", "martha");
		cedulas.put("12345678910321", "andres");
		cedulas.put("11111", "carlos");
		cedulas.put("2222", "carlos alberto");
	}
	
	private void SetupEscenario2(){
		cedulas = new RedBlackBST<String, String>();
		cedulas.put("2030445", "Luis Mar�a Mujica");
		cedulas.put("121", "Luis");
		cedulas.put("123", "andrea");
		cedulas.put("1234", "alejandra");
		cedulas.put("12345", "mar�a");
	}
	
	
	public void testTamanio(){
		SetupEscenario();
		assertEquals(14, cedulas.size());
	}
	
	public void testHeight(){
		SetupEscenario();
		assertEquals(4, cedulas.height());

	}
	
	public void testHeight2(){
		SetupEscenario();
		cedulas.put("2323112", "david fajardo");
		cedulas.put("2328973", "david andres");
		cedulas.put("2311128973", "mujica alvaro");
		cedulas.put("23289111173", "the killer");
		cedulas.put("2328811173", "lazy man");
		cedulas.put("2328900173", "smallville");
		assertEquals(5, cedulas.height());
	}
	
	public void testGet(){
		SetupEscenario();
		assertEquals("Luis", cedulas.get("121"));
		assertEquals("andrea", cedulas.get("123"));
		assertEquals("alejandra", cedulas.get("1234"));
	}
	
	public void testDelete(){
		SetupEscenario2();
		cedulas.delete("2030445");
		cedulas.delete("121");
		cedulas.delete("123");
		cedulas.delete("1234");
		cedulas.delete("12345");
		assertEquals(0, cedulas.size());
	}

}
