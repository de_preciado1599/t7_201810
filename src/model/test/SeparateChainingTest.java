package model.test;


import junit.framework.TestCase;
import model.data_structures.SeparateChainingHashTable;

public class SeparateChainingTest extends TestCase{

	private SeparateChainingHashTable<String, String> cedulas;

	private void SetupEscenario1(){
		cedulas = new SeparateChainingHashTable<String, String>(4);
		cedulas.put("2030445", "Luis Mar�a Mujica");
		cedulas.put("110112", "Luis");
		cedulas.put("123", "andrea");
		cedulas.put("1234", "alejandra");
		cedulas.put("12345", "mar�a");
		cedulas.put("123456", "Sara");
		cedulas.put("1234567", "david");
		cedulas.put("123456789999", "sdsdsdsd");
		cedulas.put("123456789", "karen");
		cedulas.put("12345678910", "mylene");
		cedulas.put("1234567891012", "martha");
		cedulas.put("12345678910321", "andres");
		cedulas.put("11111", "carlos");
		cedulas.put("2222", "carlos alberto");
	}

	public void testPutGet(){
		SetupEscenario1();
		assertEquals(cedulas.getValue("12345678910321"), "andres");
		assertEquals(cedulas.getValue("12345678910"), "mylene");
		assertEquals(cedulas.getValue("11111"), "carlos");

	}


	public void testSize(){
		SetupEscenario1();
		assertEquals(14, cedulas.size());

	}

	public void testFactorM(){
		SetupEscenario1();
		assertEquals(8, cedulas.m());
	}

	public void testContains(){
		SetupEscenario1();
		assertEquals(true, cedulas.contains("11111"));
		assertEquals(false, cedulas.contains("111111"));

	}

	//este metodo funciona si el factor de carga es mayor o igual a 2
	public void testResizingPut(){
		SetupEscenario1();
		cedulas.put("1234564589", "jos�");
		cedulas.put("1234561113", "camila ");
		cedulas.put("1234560001", "lorena");
		assertEquals(16, cedulas.m());
		assertEquals(17, cedulas.size());
	}

	public void testDelete(){
		SetupEscenario1();
		cedulas.delete("2030445");
		cedulas.delete("110112");
		cedulas.delete("123");
		cedulas.delete("1234");
		cedulas.delete("12345");
		cedulas.delete("123456");
		cedulas.delete("1234567");
		cedulas.delete("123456789999");
		cedulas.delete("123456789");
		cedulas.delete("12345678910");
		cedulas.delete("1234567891012");
		cedulas.delete("12345678910321");
		cedulas.delete("11111");
		cedulas.delete("2222");
		assertEquals(0, cedulas.size());
	}


	public void testResizingDelete(){
		SetupEscenario1();
		assertEquals(8, cedulas.m());
		cedulas.delete("123456789");
		cedulas.delete("12345678910");
		cedulas.delete("1234567891012");
		cedulas.delete("12345678910321");
		cedulas.delete("11111");
		cedulas.delete("2222");
		assertEquals(4, cedulas.m());

	}

}
