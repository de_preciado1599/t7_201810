package model.vo;

public class Servicio implements Comparable<Servicio>{



	private int dropoff_community_area;

	private double pickup_centroid_latitude;

	private double pickup_centroid_longitude;

	private int pickup_community_area;

	private String taxi_id;

	private String trip_end_timestamp;

	private String trip_id;

	private double trip_miles;

	private int trip_seconds;

	private String trip_start_timestamp;

	private double trip_total;
	
	private double distanciaHarvesianaMillas;


	
	
	public Servicio(String trip_id, String taxi_id, int trip_seconds, double trip_miles, double trip_total,
			String trip_start_timestamp, String trip_end_timestamp, int pickup_community_area,
			int dropoff_community_area, double pickup_centroid_latitude, double pickup_centroid_longitude) {
		this.trip_id = trip_id;
		this.taxi_id = taxi_id;
		this.trip_seconds = trip_seconds;
		this.trip_miles = trip_miles;
		this.trip_total = trip_total;
		this.trip_start_timestamp = trip_start_timestamp;
		this.trip_end_timestamp = trip_end_timestamp;
		this.pickup_community_area = pickup_community_area;
		this.dropoff_community_area = dropoff_community_area;
		this.pickup_centroid_latitude = pickup_centroid_latitude;
		this.pickup_centroid_longitude = pickup_centroid_longitude;
		this.distanciaHarvesianaMillas=0.0;
	}













	public String getTrip_id() {
		return trip_id;
	}













	public void setTrip_id(String trip_id) {
		this.trip_id = trip_id;
	}













	public String getTaxi_id() {
		return taxi_id;
	}













	public void setTaxi_id(String taxi_id) {
		this.taxi_id = taxi_id;
	}













	public int getTrip_seconds() {
		return trip_seconds;
	}













	public void setTrip_seconds(int trip_seconds) {
		this.trip_seconds = trip_seconds;
	}













	public double getTrip_miles() {
		return trip_miles;
	}













	public void setTrip_miles(double trip_miles) {
		this.trip_miles = trip_miles;
	}













	public double getTrip_total() {
		return trip_total;
	}













	public void setTrip_total(double trip_total) {
		this.trip_total = trip_total;
	}













	public String getTrip_start_timestamp() {
		return trip_start_timestamp;
	}













	public void setTrip_start_timestamp(String trip_start_timestamp) {
		this.trip_start_timestamp = trip_start_timestamp;
	}













	public String getTrip_end_timestamp() {
		return trip_end_timestamp;
	}













	public void setTrip_end_timestamp(String trip_end_timestamp) {
		this.trip_end_timestamp = trip_end_timestamp;
	}













	public int getPickup_community_area() {
		return pickup_community_area;
	}













	public void setPickup_community_area(int pickup_community_area) {
		this.pickup_community_area = pickup_community_area;
	}













	public int getDropoff_community_area() {
		return dropoff_community_area;
	}













	public void setDropoff_community_area(int dropoff_community_area) {
		this.dropoff_community_area = dropoff_community_area;
	}













	public double getPickup_centroid_latitude() {
		return pickup_centroid_latitude;
	}













	public void setPickup_centroid_latitude(double pickup_centroid_latitude) {
		this.pickup_centroid_latitude = pickup_centroid_latitude;
	}













	public double getPickup_centroid_longitude() {
		return pickup_centroid_longitude;
	}













	public void setPickup_centroid_longitude(double pickup_centroid_longitude) {
		this.pickup_centroid_longitude = pickup_centroid_longitude;
	}
	
	public void setDistanciaHarvesianaMillas(double pDistancia){
		this.distanciaHarvesianaMillas = pDistancia;
	}

	public double getDistanciaHarvesianaMillas(){
		return this.distanciaHarvesianaMillas;
	}












	@Override
	public int compareTo(Servicio arg0) {
		// TODO Auto-generated method stub
		return this.getTrip_start_timestamp().compareTo(arg0.getTrip_start_timestamp());
	}
}