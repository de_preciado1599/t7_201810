package model.vo;

import jdk.management.resource.internal.TotalResourceContext;

/**
 * 
 * @author Daniel Preciado
 * clase que representa un taxi con los atributos solicitados en
 * el taller numero 7
 */
public class TaxiTaller implements Comparable<TaxiTaller>
{

	//____________________________________________________________
	//Attributes
	//____________________________________________________________
	
	/**
	 * identificador asociado al taxi
	 */
	private String taxiId ;
	
	/**
	 * empresa a la que esta asociada el taxi
	 */
	private String empresaTaxi;
	
	/**
	 * cantidad de servicios asociados al taxi
	 */
	private int serviciosAsociados;
	
	/**
	 * dinero total recibido por el taxi
	 */
	private double totalDinero;
	
	/**
	 * tiempo total invertido en los servicios
	 */
	private int totalTiempo;
	
	/**
	 * distancia total recorrida por sus servicios
	 */
	private double totalDistancia;
	//____________________________________________________________
	//Methods
	//____________________________________________________________
	
	/**
	 * 
	 * @param pId
	 * @param pCompany
	 * @param pDinero
	 * @param pTiempo
	 * @param pDistancia
	 */
	public TaxiTaller(String pId, String pCompany,double pDinero,int pTiempo, double pDistancia) 
	{
		taxiId = pId;
		empresaTaxi = pCompany;
		aumentarServicios();
		totalDinero = pDinero;
		totalTiempo = pTiempo;
		totalDistancia = pDistancia;
	}
	
	/**
	 * aumenta la cantidad de servicios asociados en 1
	 */
	private void aumentarServicios() 
	{
		serviciosAsociados = serviciosAsociados + 1;
	}
	
	/**
	 * 
	 * @param pDinero
	 */
	public void aumentarDinero (double pDinero) 
	{
		totalDinero = totalDinero + pDinero;
	}
	
	/**
	 * 
	 * @param pTiempo
	 */
	public void aumentarTiempo (int pTiempo) 
	{
		totalTiempo = totalTiempo + pTiempo;
	}
	
	/**
	 * 
	 * @param pDistancia
	 */
	public void aumentarDistancia (double pDistancia) 
	{
		totalDistancia = totalDistancia + pDistancia;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getTaxiId() 
	{
		return taxiId;
	}
	
	public String getEmpresaTaxi() {
		return empresaTaxi;
	}

	public void setEmpresaTaxi(String empresaTaxi) {
		this.empresaTaxi = empresaTaxi;
	}

	public int getServiciosAsociados() {
		return serviciosAsociados;
	}

	public double getTotalDinero() {
		return totalDinero;
	}

	public int getTotalTiempo() {
		return totalTiempo;
	}

	public double getTotalDistancia() {
		return totalDistancia;
	}

	@Override
	public int compareTo(TaxiTaller o) 
	{
		// TODO Auto-generated method stub
		return taxiId.compareTo(o.taxiId);
	}

}
