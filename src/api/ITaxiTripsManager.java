package api;

import model.data_structures.IList;
import model.data_structures.LinkedList;
import model.vo.Servicio;
import model.vo.TaxiTaller;

/**
 * API para la clase de logica principal  
 */
public interface ITaxiTripsManager 
{
	/**
	 * Dada la direccion del json que se desea cargar, se generan vo's, estructuras y datos necesarias
	 * @param direccionJson, ubicacion del json a cargar
	 * @return true si se lo logro cargar, false de lo contrario
	 */
	public boolean cargarSistema(String direccionJson);
	
	/**
	 * da informacion general
	 */
	public void darInformacion(); 
	
	/**
	 * Consultar la informaci�n asociada a un taxi dado su Id
	 */
	public TaxiTaller darInformacionAsociadaTaxi(String pId) ;
	
	/**
	 * Consultar los Ids de los taxis que se encuentren registrados con Ids en un rango dado por
	 *[Idmenor, IdMayor]. Se espera hacer una b�squeda eficiente de los Ids de los taxis en el rango
	 *(Debe intentarse No recorrer todo el �rbol).
	 */
	public LinkedList<String> darLLavesTaxisEnRango(String pIdMenor, String pIdMayor);

}